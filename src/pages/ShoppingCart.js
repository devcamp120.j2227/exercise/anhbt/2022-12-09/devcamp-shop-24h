import { faTrashCan } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decrementQuantity, incrementQuantity, removeItem } from '../features/cart/cartSlice';

function ShoppingCart() {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart.cart)
  const total = cart.reduce((accumulator, item) => accumulator + item.Price * item.quantity, 0)
  return (
    <div className='m-12'>
      <h1 className='text-2xl text-center'>Shopping Cart</h1>
      {
        cart.length !== 0 ?
        cart.map((item, index) => {
        return <div key={index} className='mx-auto mt-2 pb-4 border-b max-w-sm'>
          <div className='text-lg py-1'>{index + 1}. <img alt={item.Title} className='inline w-16' src={item.ImageUrl}/> {item.Title} ${item.Price}</div>
          <div className='flex justify-between'>
            <div>
              <button onClick={()=>{dispatch(removeItem(item._id))}} className='btn btn-square btn-error'><FontAwesomeIcon icon={faTrashCan}/></button>
            </div>
            <div>
              <button onClick={()=>{dispatch(decrementQuantity(item._id))}} className='btn btn-square btn-accent'>-</button>
              &nbsp;x{item.quantity}&nbsp;
              <button onClick={()=>{dispatch(incrementQuantity(item._id))}} className='btn btn-square btn-accent'>+</button>
            </div>
          </div>
        </div>
        })
        :
        <>Cart is empty</>
      }
      {
        total ? <div className='max-w-sm mx-auto'>
          <div className='mt-2 text-2xl'>
            Total : ${total}
          </div>
          <button className='btn btn-accent'>
            Order
          </button>
        </div>
        :
        null
      }
    </div>
  )
}

export default ShoppingCart