import React, { useEffect, useState } from 'react'
import BreadCrumb from '../components/Breadcrumb'
import ProductFilter from '../components/products/ProductFilter'
import { useDispatch, useSelector } from 'react-redux'
import ProductCard from '../components/products/ProductCard'
import { updateCurrentPage } from '../features/filter/filterSlice'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGear } from '@fortawesome/free-solid-svg-icons'
const URL = `http://127.0.0.1:8000/`
var requestOptions = {
  method: 'GET',
  redirect: 'follow'
};
function ProductList() {
  const dispatch = useDispatch()
  const crumbs = [{
    name: 'Trang chủ',
    url: '/'
  }, {
    name: 'Danh mục sản phẩm',
    url: '#'
  }]
  const [pages, setPages] = useState(0)
  const [products, setProducts] = useState([])
  const filter = useSelector((state)=>state.filter)
  useEffect(() => {
    fetch( URL+
      "products/filter"+
      `?Title=${filter.name}&minPrice=${filter.minPrice}&maxPrice=${filter.maxPrice}&Category=${filter.type}&currentPage=${filter.currentPage}`, requestOptions)
    .then(response => response.json())
    .then(result => {
      setProducts(result.data)
      setPages(result.pages)
    })
    return () => {}
  }, [filter])

  return (
    <>
        <BreadCrumb crumbs={crumbs} />
        <div className="dropdown  sm:hidden  sticky top-20">
          <label tabIndex={0} className="btn m-1"><FontAwesomeIcon icon={faGear }/></label>
          <div tabIndex={0} className="dropdown-content p-2 ml-1 rounded-box border-2 bg-base-100">

            <ProductFilter/>
          </div>
        </div>
        <div className='flex '>
          <div className='hidden sm:block sm:w-auto flex-none'>
            <ProductFilter/>
          </div>
            <div className='mx-auto grid grid-cols-1 md:grid-cols-2 md:grid-rows-4 lg:grid-cols-4 lg:grid-rows-2 gap-4'>
              {!products ? null : products.map(function(product, index){
                  return (
                    <ProductCard key={index} productDetail = {product}/>
                  )
              })}
            </div>
        </div>
        <div className='my-5 w-full'>
            <div className="btn-group flex justify-center">
              { products? 
              [...Array(pages)].map((page, index)=>{
                return <button key={index} onClick={()=>{
                  dispatch(updateCurrentPage(index+1))
                }} className="btn">{index + 1}</button>
              }) : null
              }
            </div>
        </div>
    </>
  )
}

export default ProductList