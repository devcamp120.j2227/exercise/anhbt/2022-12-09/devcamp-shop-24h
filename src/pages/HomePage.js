import React from 'react'
import FeaturedCarousel from '../components/Content/FeaturedCarousel'
import FeaturedProducts from '../components/Content/FeaturedProducts'


function HomePage() {
  return (
    <div className='sm:mx-4 md:mx-16 lg:mx-20 my-4'>
        <FeaturedCarousel></FeaturedCarousel>
        <div className='divider'></div>
        <FeaturedProducts></FeaturedProducts>
    </div>
  )
}

export default HomePage