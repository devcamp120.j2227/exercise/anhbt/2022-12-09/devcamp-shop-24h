import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import AddToCartButton from '../components/AddToCartButton'
import BreadCrumb from '../components/Breadcrumb'
import ProductCard from '../components/products/ProductCard'

function ProductInfo() {
    const {productId} = useParams()
    const [product, setProduct] = useState()
    const [similarProducts, setSimilarProducts] = useState()
    useEffect(() => {
        fetch(`http://127.0.0.1:8000/products/${productId}`, {
            method: 'GET',
            redirect: 'follow'
        })
        .then(response => response.json())
        .then(result => setProduct(result.data))
    }, [productId])
    useEffect(() => {
        fetch(`http://127.0.0.1:8000/products/${productId}`, {
            method: 'GET',
            redirect: 'follow'
        })
        .then(response => response.json())
        .then(result => setProduct(result.data))
    }, [productId])

    const crumbs = [{
    name: 'Trang chủ',
    url: '/'
    },{
    name: 'Danh mục sản phẩm',
    url: '/products'
    },{
    name: 'Chi tiết sản phẩm',
    url: '#'
    }]
    return (
        <>
            <BreadCrumb crumbs={crumbs} />
            {product ?
            <>
            <div className='flex flex-col sm:flex-row p-4'>
                <div className='flex flex-col justify-center mx-auto'>
                    <img 
                    className='max-w-sm'
                    src={product.ImageUrl} 
                    alt={`${product.Manufacturer} ${product.Title}`} />
                </div>
                <div className='text-center sm:text-left'>
                    <p className='md:text-lg font-semibold'>{product.Manufacturer}&trade;</p>
                    <p className='mb-2 text-2xl md:text-4xl font-semibold mt-1'>{product.Title} - ${product.Price}</p>
                    <AddToCartButton product={product}/>
                    <p className='mt-3 text-justify'>{product.Description}</p> 
                </div>
            </div>
            <div>
                <p className='md:text-lg font-semibold'>Similar products</p>
                <ProductCard productDetail={product}></ProductCard>
            </div>
            </>
            : null
            }
        </>
    )
}

export default ProductInfo