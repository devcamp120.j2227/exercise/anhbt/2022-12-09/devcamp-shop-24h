import { faGoogle } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import Logo from '../components/Header/Logo'
import {  GoogleAuthProvider, signInWithPopup} from "firebase/auth";
import { updateUser } from '../features/user/userSlice';
import auth from '../firebase';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

const provider = new GoogleAuthProvider();
function Login() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const loginGoogle = () => {
    signInWithPopup(auth, provider)
        .then((result) => {
        console.log(result);
        dispatch( updateUser(result.user));
        navigate("/")
        })
        .catch((error) => {
        console.error(error);
        })
    }
    return (
    <div className=' my-12 w-80 mx-auto text-center'>
        <div className="form text-center flex flex-col align-middle">
            <Logo />
            <div className="form-control w-full">
                <label className="label">
                    <span className="label-text">Username</span>
                </label>
                <input type="text" className="input input-bordered w-full" />
            </div>
            <div className="form-control w-full">
                <label className="label">
                    <span className="label-text">Password</span>
                </label>
                <input type="password" className="input input-bordered w-full" />
            </div>
        </div>
        <div className='flex flex-col'>
            <button type='button' className="mt-3 btn btn-success">Enter</button>
            <button type='button' onClick={loginGoogle} className="mt-3 btn btn-error"><FontAwesomeIcon icon={faGoogle}/>&nbsp; Sign in with Google</button>
        </div>

    </div>
  )
}

export default Login