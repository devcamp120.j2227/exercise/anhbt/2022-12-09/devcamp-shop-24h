import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  name: '',
  minPrice: '',
  maxPrice: '',
  type: '',
  currentPage: 1
}

export const filterSlice = createSlice({
  name: 'filter',
  initialState,
  reducers: {
    updateFilter(state, action) {
        const { name, minPrice, maxPrice, type } = action.payload
        Object.assign(state, { name, minPrice, maxPrice, type, currentPage : 1 })
    },
    updateCurrentPage(state, action) {
        Object.assign(state, { currentPage : action.payload })
    }
  },
})

export const { updateFilter, updateCurrentPage } = filterSlice.actions

export default filterSlice.reducer