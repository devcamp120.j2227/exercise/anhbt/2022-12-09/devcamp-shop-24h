import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBa1IRZGfQygenrcfrvKo83FwWmWTR3gfw",
  authDomain: "devcamp-buitienanh.firebaseapp.com",
  projectId: "devcamp-buitienanh",
  storageBucket: "devcamp-buitienanh.appspot.com",
  messagingSenderId: "439840356228",
  appId: "1:439840356228:web:1201b9e225b87da10d4fd1"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

export default auth