import { Toaster } from 'react-hot-toast';
import './App.css';
import ContentRoutes from './components/ContentRoutes';
import MyFooter from './components/Footer';
import MyHeader from './components/Header';

function App() {
  return (
    <div className='flex flex-col h-screen'>
      <Toaster
      toastOptions={{duration: 2000}}/>
      <MyHeader></MyHeader>
      <ContentRoutes></ContentRoutes>
      <MyFooter></MyFooter>
    </div>
  );
}

export default App;
