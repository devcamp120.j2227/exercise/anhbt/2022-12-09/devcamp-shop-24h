import React from 'react'
import { Route, Routes } from 'react-router-dom'
import routerList from '../routes'

function ContentRoutes() {
  return (
    <>
          <div className='pt-0'>
            <Routes>
              {routerList.map((router, index) => {
                return (router.path) ? 
                <Route 
                  key={index} 
                  exact 
                  path={router.path} 
                  element={router.element}
                /> 
                :
                null
              })}
            </Routes>
          </div>
    </>
  )
}

export default ContentRoutes