import React from 'react'
import { useNavigate } from 'react-router-dom'

function BreadCrumb({crumbs}) {
  const navigate = useNavigate();
  return (
    <div className="ml-4 text-sm breadcrumbs">
        <ul>{crumbs.map((crumb, index)=>{
          return <li key={index}>
            <button className=' hover:underline' onClick={()=>{navigate(crumb.url)}}>
              {crumb.name}</button></li>
          })}</ul>
    </div>
  )
}

export default BreadCrumb