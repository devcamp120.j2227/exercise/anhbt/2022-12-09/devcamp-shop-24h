import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faInstagram, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons'
import Logo from '../Header/Logo'

function SocialFooter() {
  return (
        <div className='text-right mt-5 '>
            <Logo></Logo>
            <ul className="mt-3 flex flex-row-reverse gap-4 px-4 py-0">
                <li className='hover:text-lime-500 active:text-amber-400'>
                    <a href="/">
                        <FontAwesomeIcon icon={faFacebook} size="lg"/>
                    </a>
                </li>
                <li className='hover:text-lime-500 active:text-amber-400'>
                    <a href="/">
                        <FontAwesomeIcon icon={faInstagram} size="lg"/>
                    </a>
                </li>
                <li className='hover:text-lime-500 active:text-amber-400'>
                    <a href="/">
                        <FontAwesomeIcon icon={faYoutube} size="lg"/>
                    </a>
                </li>
                <li className='hover:text-lime-500 active:text-amber-400'>
                    <a href="/">
                        <FontAwesomeIcon icon={faTwitter} size="lg"/>
                    </a>
                </li>
            </ul>
        </div>
  )
}

export default SocialFooter