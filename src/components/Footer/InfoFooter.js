import React from 'react'

function InfoFooter() {
  return (
    <div className='grid grid-cols-3 text-sm sm:text-base'>
        <div className='grid '>
                <div className='font-bold'>PRODUCTS</div>
            </div>
            <div className='grid '>
                <div className='font-bold'>SERVICES</div>
            </div>
            <div className='grid '>
                <div className='font-bold'>SUPPORT</div>
            </div>
    </div>
  )
}

export default InfoFooter