import { faLeaf } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { useNavigate } from 'react-router-dom'
function Logo() {
const navigate = useNavigate()

  return (
    <>
      <button onClick={()=> { navigate("/")
      }} className="text-xl px-2 font-black text-lime-600"><span className='text-lime-800'><FontAwesomeIcon icon={faLeaf} size="lg"/></span>Shop24h</button>
    </>
        
  )
}

export default Logo