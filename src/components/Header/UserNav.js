import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faShoppingCart, faUser } from "@fortawesome/free-solid-svg-icons"
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import auth from '../../firebase'
import { updateUser } from '../../features/user/userSlice'
import { signOut } from 'firebase/auth'


function UserNav() {
    const dispatch = useDispatch();
    const user = useSelector((state) => state.user.user)
    const navigate = useNavigate();
    const onUserClick = () => {
        if (!user)  {
            navigate("/login")
        }
    }
    const logout = () => {
        signOut(auth)
          .then(() => {
            dispatch (updateUser(null));
          })
          .catch((error) => {
            console.error(error);
          })
      }
  return (
    <div className="text-right flex text-lime-600">
        <ul className="flex flex-row gap-6">
            <li>
                <button className='hover:text-lime-500 active:text-amber-400' onClick={onUserClick}>
                    <FontAwesomeIcon icon={faUser} size="lg"></FontAwesomeIcon>
                </button>
            </li>
            <li>
                <button className='hover:text-lime-500 active:text-amber-400' onClick={()=>{navigate("/cart")}}>
                    <FontAwesomeIcon icon={faShoppingCart} size="lg"></FontAwesomeIcon>
                </button>
            </li>
            <li>
                { user ? 
                <>
                    <div className="dropdown dropdown-hover dropdown-bottom dropdown-end">
                        <label tabIndex={0}  className='text-sm' data-dropdown-toggle="dropdown">
                            <img className='w-6 rounded-full inline' src={user.photoURL} referrerPolicy='no-referrer' alt='user'/>
                            <span className='hidden sm:inline'> {user.displayName}</span>
                            
                        </label>
                        <ul tabIndex={0} className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52 ">
                            <li><button onClick={logout}>Sign out</button></li>
                        </ul>
                    </div>
                </>
                
                : null
                }
                
            </li>
        </ul>
    </div>
  )
}

export default UserNav