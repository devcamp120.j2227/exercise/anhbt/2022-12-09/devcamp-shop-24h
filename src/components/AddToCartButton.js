import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { toast } from 'react-hot-toast';
import { useDispatch } from 'react-redux'
import { addToCart } from '../features/cart/cartSlice';

function AddToCartButton(productProp) {
    const dispatch = useDispatch();
    return (
        <button className='w-32 bg-lime-700 mt-2' onClick={
            () =>{
                toast("Đã cho hàng vào giỏ");
                dispatch(addToCart(productProp.product))
        }}>
            <span className=' block bg-lime-600 -translate-y-2 text-amber-300 p-4 text-center text-lg transition ease-in-out duration-300 hover:bg-lime-500 hover:text-amber-200 active:-translate-y-1'>
                <FontAwesomeIcon icon={faShoppingCart}></FontAwesomeIcon> ${productProp.product.Price}.00
            </span>
        </button>
    )
}

export default AddToCartButton