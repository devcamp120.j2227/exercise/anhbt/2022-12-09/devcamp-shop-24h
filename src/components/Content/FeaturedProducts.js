import React, { useEffect, useState } from 'react'
import ProductCard from '../products/ProductCard'

var requestOptions = {
  method: 'GET',
  redirect: 'follow'
};

function FeaturedProducts() {
  const [featuredProducts, setFeaturedProducts] = useState([])
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    fetch("http://127.0.0.1:8000/products?limit=8", requestOptions)
    .then(response => response.json())
    .then(result => setFeaturedProducts(result.data))
  
    return () => {setLoaded(true)}
  }, [])
  return (
    <div className='mt-10 text-center'>
        <h2 className='text-2xl font-extrabold'>LATEST PRODUCTS</h2>
        <div className='w-16 my-2 mx-auto border-2 border-lime-600'/>
        <div className='mt-5 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-2 p-4'>
            { loaded? featuredProducts.map(function(product, index){
                return (
                    <ProductCard key={index} productDetail = {product}/>
                )
            }) : null
            }
        </div>
    </div>
  )
}

export default FeaturedProducts