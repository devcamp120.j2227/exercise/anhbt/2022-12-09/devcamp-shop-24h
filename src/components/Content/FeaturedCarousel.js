import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Carousel } from 'flowbite-react'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import AddToCartButton from '../AddToCartButton'

var requestOptions = {
  method: 'GET',
  redirect: 'follow'
};

function FeaturedCarousel() {
  const [featuredProducts, setFeaturedProducts] = useState([])
  useEffect(() => {
    fetch("http://127.0.0.1:8000/products?limit=3", requestOptions)
    .then(response => response.json())
    .then(result => setFeaturedProducts(result.data))
  
    return () => {}
  }, [])
  
  const navigate = useNavigate();
  return (
    <div className=" h-96">
      <Carousel 
        slideInterval={10000}
        leftControl={<div className='text-lime-600 transition ease-in-out duration-300 hover:scale-125 active:scale-100'><FontAwesomeIcon icon={faAngleLeft} size="2x"></FontAwesomeIcon></div>}
        rightControl={<div className='text-lime-600 transition ease-in-out duration-300 hover:scale-125 active:scale-100'><FontAwesomeIcon icon={faAngleRight} size="2x"></FontAwesomeIcon></div>}
      >
        {featuredProducts ? featuredProducts.map(function(product, index){
          return (
            <div key={index} className='mt-4 px-4 md:px-20 grid grid-cols-2 justify-center'>

              <div className='pl-16 flex flex-col justify-center z-10'>
                <p className='font-semibold'>{product.Manufacturer}&trade;</p>
                <p className='text-2xl md:text-4xl font-semibold mt-1'>{product.Title}</p>
                <p className=' hidden md:block h-12 mt-3 overflow-hidden break-words'>{product.Description}</p>
                <div className='mt-2'>
                  <AddToCartButton product={product}/>
                </div>
              </div>

              <button onClick={()=>{navigate(`/products/${product._id}`)}} className='flex flex-col justify-center'>
                <img
                  className='max-h-64 mx-auto inline-block overflow-hidden transition ease-in-out duration-500 hover:scale-110 active:scale-100'
                  src={product.ImageUrl}
                  alt={product.Manufacturer +" "+product.Title}
                />
              </button>
            </div>
          )
        }) : null
      }
      </Carousel>
    </div>
  )
}
export default FeaturedCarousel