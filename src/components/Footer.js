import React from 'react'

import InfoFooter from './Footer/InfoFooter'
import SocialFooter from './Footer/SocialFooter'


function MyFooter() {
  return (
    <footer className="bg-gray-200 px-4 py-4 mt-auto">
        <div className = "container mx-auto grid grid-cols-5">
            <div className='col-span-3'>
                <InfoFooter></InfoFooter>
            </div>
            <div className='col-span-2'>
                <SocialFooter></SocialFooter>
            </div>
        </div>
    </footer>
  )
}

export default MyFooter