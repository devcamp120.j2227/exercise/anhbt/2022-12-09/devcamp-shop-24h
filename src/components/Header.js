import React from 'react'
import Logo from './Header/Logo'
import UserNav from './Header/UserNav'
import { useNavigate } from 'react-router-dom'


function MyHeader() {
const navigate = useNavigate()
const onProductsClick = () => { navigate("/products")
}
    return (
        <>
            <nav className=" bg-lime-300 px-4 py-2.5 sticky w-full z-20 top-0 left-0 border-b border-gray-200">
                <div className="flex py-4">
                    <Logo></Logo>
                    <div className='mx-8 flex-grow'>
                        <button className='mt-1 transition duration-300 hover:underline' onClick={onProductsClick} >Products</button>
                    </div>
                    <UserNav></UserNav>
                </div>
            </nav>
        </>
        
    )
}

export default MyHeader