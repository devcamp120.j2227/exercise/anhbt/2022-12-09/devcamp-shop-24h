import React from 'react'
import { useNavigate } from 'react-router-dom'

function ProductCard({productDetail}) {
    const navigate = useNavigate();
    return (
    <div className='w-72 sm:w-64 md:w-60 lg:w-56 mx-auto flex flex-col transition duration-300 border-b-4 border-lime-600 hover:border-lime-400 text-center'>
        <button onClick={()=>{navigate(`/products/${productDetail._id}`)}} className='flex flex-col justify-center h-52 overflow-hidden border-b-0 border-gray-100'>
            <img 
                className='mx-auto h-48 inline-block transition ease-in-out duration-500 hover:scale-110 active:scale-100'
                src={productDetail.ImageUrl}
                alt={productDetail.Manufacturer +" "+productDetail.Title}
            />
        </button>
        <p className='text-sm'>{productDetail.Manufacturer}&trade;</p>
        <p className='text-lg font-bold'>{productDetail.Title}</p>
        <div className='flex justify-center gap-4'>
            {productDetail.OrgPrice > productDetail.Price 
                ? 
                <>
                    <s>${productDetail.OrgPrice}</s>
                    <p>${productDetail.Price}</p>
                </>
                :
                <p>${productDetail.OrgPrice}</p>
            }
        </div>
    </div>
    )
}

export default ProductCard