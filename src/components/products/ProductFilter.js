import { faFilter } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react'
import { useForm } from "react-hook-form";
import { useDispatch } from 'react-redux';
import { updateFilter } from '../../features/filter/filterSlice';
function ProductFilter() {
  const dispatch = useDispatch();
  const { register, handleSubmit } = useForm();
  const onSubmit = data => {
    if (!data.type) data = {...data, type : ''}
    dispatch(updateFilter(data));
  };

  return (
    <form className='p-4' onSubmit={handleSubmit(onSubmit)}>
      <div className='font-bold mb-2'>Name</div>
      <ul>
        <li className='mt-1'><input type="text" {...register("name")} className="input input-bordered input-sm w-40" /></li>
      </ul>

      <br></br>

      <div className='font-bold mb-2'>Price</div>
      <ul>
        <li className='mt-1'>
          <input type="text" {...register("minPrice")} className="input input-bordered input-sm w-16" />
          &nbsp;-&nbsp; 
          <input type="text" {...register("maxPrice")} className="input input-bordered input-sm w-16" />
          </li>
      </ul>

      <br></br>

      <div className='font-bold mb-2'>Category</div>
      <ul>
        <li className='mt-1'><input type="radio" name='type' value='' {...register("type")} className="radio"/> All</li>
        <li className='mt-1'><input type="radio" name='type' value='Fruit' {...register("type")} className="radio" /> Fruit</li>
        <li className='mt-1'><input type="radio" name='type' value='Bakery' {...register("type")} className="radio" /> Bakery</li>
        <li className='mt-1'><input type="radio" name='type' value='Meat' {...register("type")} className="radio" /> Meat</li>
        <li className='mt-1'><input type="radio" name='type' value='Dairy' {...register("type")} className="radio" /> Dairy</li>
      </ul>

      <br></br>
      <button type='submit' className="btn"><FontAwesomeIcon icon={faFilter}/>&nbsp; Filter</button>
    </form>
  )
}

export default ProductFilter