import { configureStore } from '@reduxjs/toolkit'
import filterReducer from '../features/filter/filterSlice'
import userReducer from '../features/user/userSlice'
import cartReducer from '../features/cart/cartSlice'

export const store = configureStore({
  reducer: {
    filter: filterReducer,
    user: userReducer,
    cart: cartReducer
  },
})