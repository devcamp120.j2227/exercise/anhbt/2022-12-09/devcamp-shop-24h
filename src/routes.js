import HomePage from "./pages/HomePage"
import Login from "./pages/Login"
import ProductInfo from "./pages/ProductInfo"
import ProductList from "./pages/ProductList"
import ShoppingCart from "./pages/ShoppingCart"


const routerList = [
    { label: "Home", path: "/",  element: <HomePage />},
    { label: "Product", path: "/products", element: <ProductList/>},
    { label: "ProductInfo", path: "/products/:productId", element: <ProductInfo/>},
    { label: "ShoppingCart", path: "/cart", element: <ShoppingCart/>},
    { label: "Login", path: "/login", element: <Login/>}
]

export default routerList